package com.pms.sdk.api.request;

import java.net.URI;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.Badge;

public class DeviceCert extends BaseRequest {

	public DeviceCert(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (JSONObject userData) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();

			// new version
			jobj.put("appKey", PMSUtil.getApplicationKey(mContext));
			/**
			 * 2019.10.23 Android 10 IMEI값 사용불가 이슈에 따른 대응
			 */
			String savedUuid = PMSUtil.getUUID(mContext);
			String uuidByAndroidId = PhoneState.generateUUID(mContext);
			if(TextUtils.isEmpty(savedUuid))
			{
				CLog.i("this user has not uuid, so it create uuid by androidId");
				savedUuid = uuidByAndroidId;
				PMSUtil.setUUID(mContext, savedUuid);
			}
			else
			{
				CLog.i("this user has uuid ("+savedUuid+")");
			}
			jobj.put("uuid", savedUuid);
			jobj.put("androidId", uuidByAndroidId);
			jobj.put("pushToken", PMSUtil.getGCMToken(mContext));
			jobj.put("custId", PMSUtil.getCustId(mContext));
			jobj.put("appVer", PhoneState.getAppVersion(mContext));
			jobj.put("os", "A");
			jobj.put("osVer", PhoneState.getOsVersion());
			jobj.put("device", PhoneState.getDeviceName());
			jobj.put("sessCnt", "1");

			if (userData != null) {
				jobj.put("userData", userData);
			}

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final JSONObject userData, final APICallback apiCallback)
	{
		try
		{
			String custId = PMSUtil.getCustId(mContext);

//			boolean isNExistsUUID = PhoneState.createDeviceToken(mContext);
//			CLog.i("Device Cert request get UUID : " + (isNExistsUUID == true ? "Created" : "Exists"));

			String token = PMSUtil.getGCMToken(mContext);
			if (TextUtils.isEmpty(token) || NO_TOKEN.equals(token)) {
				CLog.i("DeviceCert:no push token");
				mPrefs.putString(KEY_GCM_TOKEN, NO_TOKEN);
			}

			if (!custId.equals(mPrefs.getString(PREF_LOGINED_CUST_ID)))
			{
				// 기존의 custId와 auth할려는 custId가 다르다면, DB초기화
				CLog.i("DeviceCert:new user");
				mDB.deleteAll();
			}

			apiManager.call(API_DEVICE_CERT, getParam(userData), new APICallback()
			{
				@Override
				public void response (String code, JSONObject json)
				{
					if (CODE_SUCCESS.equals(code))
					{
						PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_COMPLETE);
						requiredResultProc(json);
					}

					if (apiCallback != null)
					{
						apiCallback.response(code, json);
					}
				}
			});
		}
		catch (Exception e)
		{
			CLog.e(e.getMessage());
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	@SuppressLint("DefaultLocale")
	private boolean requiredResultProc (JSONObject json) {
		try {

			PMSUtil.setAppUserId(mContext, json.getString("appUserId"));
			PMSUtil.setEncKey(mContext, json.getString("encKey"));

			// set msg flag
			mPrefs.putString(PREF_MSG_FLAG, json.getString("msgFlag"));
			mPrefs.putString(PREF_NOTI_FLAG, json.getString("notiFlag"));
			mPrefs.putString(PREF_API_LOG_FLAG, json.getString("collectApiLogFlag"));
			mPrefs.putString(PREF_PRIVATE_LOG_FLAG, json.getString("collectPrivateLogFlag"));
			String custId = PMSUtil.getCustId(mContext);
			if (!StringUtil.isEmpty(custId)) {
				mPrefs.putString(PREF_LOGINED_CUST_ID, PMSUtil.getCustId(mContext));
			}

//			// set badge
//			Badge.getInstance(mContext).updateBadge(json.getString("newMsgCnt"));

			// mqttFlag Y/N
			if (FLAG_Y.equals(PMSUtil.getMQTTFlag(mContext))) {
				String flag = json.getString("privateFlag");
				mPrefs.putString(PREF_PRIVATE_FLAG, flag);
				if (FLAG_Y.equals(flag)) {
					String protocol = json.getString("privateProtocol");	// T
					String protocolTemp = "";
					try {
						URI uri = new URI(PMSUtil.getMQTTServerUrl(mContext)); // ssl:xxx.xxx.xx:xx
						if (protocol.equals(PROTOCOL_TCP)) {
							protocolTemp = protocol.toLowerCase() + "cp";
						} else if (protocol.equals(PROTOCOL_SSL)) {
							protocolTemp = protocol.toLowerCase() + "sl";
						}

						if (uri.getScheme().equals(protocolTemp)) {				// tcp.equals(ssl)
							mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
						} else {
							mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);	// T
//							mContext.stopService(new Intent(mContext, MQTTService.class));
						}
					} catch (NullPointerException e) {
						mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
					}
//					int device = Build.VERSION.SDK_INT;
//					CLog.d("Device build version: " + device + ", Release version: " + Build.VERSION.RELEASE);
//					if (device < Build.VERSION_CODES.M && device < 23)
//					{
//						Intent intent = new Intent(mContext, RestartReceiver.class);
//						intent.setAction(ACTION_START);
//						mContext.sendBroadcast(intent);
//					}
				}
			}

			// LogFlag Y/N
			if ((FLAG_N.equals(mPrefs.getString(PREF_API_LOG_FLAG)) && FLAG_N.equals(mPrefs.getString(PREF_PRIVATE_LOG_FLAG))) == false) {
				setCollectLog();
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private void setCollectLog () {
		boolean isAfter = false;
		String beforeDate = mPrefs.getString(PREF_YESTERDAY);
		String today = DateUtil.getNowDateMo();

		try {
			isAfter = DateUtil.isDateAfter(beforeDate, today);
		} catch (Exception e) {
			isAfter = false;
		}

		if (isAfter) {
			mPrefs.putBoolean(PREF_ONEDAY_LOG, false);
		}

		if (mPrefs.getBoolean(PREF_ONEDAY_LOG) == false) {
			if (beforeDate.equals("")) {
				beforeDate = DateUtil.getNowDateMo();
				mPrefs.putString(PREF_YESTERDAY, beforeDate);
			}
			new CollectLog(mContext).request(beforeDate, null);
		}
	}
}
