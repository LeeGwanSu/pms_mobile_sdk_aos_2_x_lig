package com.pms.sdk.push.badge;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import java.util.Arrays;
import java.util.List;

public class SamsungBadger extends IBadge
{
    @Override
    public void executeBadge(Context context, int badgeCount)
    {
        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        if(launchIntent != null)
        {
            ComponentName componentName = launchIntent.getComponent();
            Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
            intent.putExtra(INTENT_EXTRA_PACKAGENAME, componentName.getPackageName());
            intent.putExtra(INTENT_EXTRA_ACTIVITY_NAME, componentName.getClassName());
            intent.putExtra(INTENT_EXTRA_BADGE_COUNT, badgeCount);

            PackageManager packageManager = context.getPackageManager();
            List<ResolveInfo> receivers = packageManager.queryBroadcastReceivers(intent, 0);
            if(receivers.size() > 0)
            {
                for (ResolveInfo info : receivers) {
                    Intent actualIntent = new Intent(intent);

                    if (info != null)
                    {
                        actualIntent.setPackage(info.resolvePackageName);
                        context.sendBroadcast(actualIntent);
                    }
                }
            }
        }
    }

    @Override
    public List<String> getLaunchers()
    {
        return Arrays.asList(
                "com.sec.android.app.launcher",
                "com.sec.android.app.twlauncher"
                );
    }
}
