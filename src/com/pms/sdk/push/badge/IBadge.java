package com.pms.sdk.push.badge;

import android.content.Context;

import java.util.List;

public abstract class IBadge
{
    protected static final String INTENT_EXTRA_BADGE_COUNT = "badge_count";
    protected static final String INTENT_EXTRA_PACKAGENAME = "badge_count_package_name";
    protected static final String INTENT_EXTRA_ACTIVITY_NAME = "badge_count_class_name";

    public abstract void executeBadge(Context context, int badgeCount);

    public abstract List<String> getLaunchers();
}
