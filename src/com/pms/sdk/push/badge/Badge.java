package com.pms.sdk.push.badge;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import com.pms.sdk.common.util.CLog;

import java.util.LinkedList;
import java.util.List;

public class Badge
{
    private static final List<Class<? extends IBadge>> BADGES = new LinkedList<>();

    static
    {
        BADGES.add(LGBadger.class);
        BADGES.add(SamsungBadger.class);
    }

    private static boolean isSupportOS;
    private static IBadge bager;

    private static void initBadge(Context context)
    {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        ResolveInfo resolveInfo = context.getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        String currentHomePackage = resolveInfo.activityInfo.packageName;

        for (Class<? extends IBadge> cls : BADGES)
        {
            IBadge b = null;
            try
            {
                b = cls.newInstance();
            }
            catch (Exception e) { }

            if (b == null)
            {
                isSupportOS = false;
                return;
            }

            if (b.getLaunchers().contains(currentHomePackage))
            {
                isSupportOS = true;
                bager = b;

                break;
            }
        }
    }

    public static void show(Context context, int count)
    {
        initBadge(context);

        if (!isSupportOS)
        {
            CLog.w("Not Support OS");
            return;
        }

        bager.executeBadge(context, count);
    }
}
