package com.pms.sdk;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.TextView;
import com.pms.sdk.bean.DelMsg;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.*;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.PushReceiver;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.Badge;

import java.io.Serializable;

public class PMS implements IPMSConsts, Serializable {

	private static final long serialVersionUID = 1L;
	private static PMS instancePms;
	private static PMSPopup instancePmsPopup;
	private Context mContext;

	private final PMSDB mDB;
	private final Prefs mPrefs;

	private OnReceivePushListener onReceivePushListener;

	/**
	 * constructor
	 * 
	 * @param context
	 */
	private PMS(Context context) {
		this.mDB = PMSDB.getInstance(context);
		this.mPrefs = new Prefs(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:"+PMS_VERSION_UPDATE_DATE);

		initOption(context);
	}

	/**
	 * initialize option
	 */
	private void initOption (Context context) {
		if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
			mPrefs.putString(PREF_NOTI_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MSG_FLAG))) {
			mPrefs.putString(PREF_MSG_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
			mPrefs.putString(PREF_RING_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
			mPrefs.putString(PREF_VIBE_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
			mPrefs.putString(PREF_ALERT_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MAX_USER_MSG_ID))) {
			mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
			mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, "Y");
		}
		if (StringUtil.isEmpty(PMSUtil.getServerUrl(context))) {
			PMSUtil.setServerUrl(context, API_SERVER_URL);
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY))) {
			mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, DEFAULT_PUSH_POPUP_ACTIVITY);
		}
		if (mPrefs.getInt(PREF_PUSH_POPUP_SHOWING_TIME) < 1) {
			mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, 10000);
		}
	}

	private void setmContext (Context context) {
		this.mContext = context;
	}

	private void unregisterReceiver () {
		Badge.getInstance(mContext).unregisterReceiver();
	}

	/**
	 * getInstance
	 * 
	 * @param context
	 * @param projectId
	 * @return
	 */
	public static PMS getInstance (Context context, String projectId) {
		PMSUtil.setGCMProjectId(context, projectId);
		return getInstance(context);
	}

	/**
	 * getInstance
	 * 
	 * @param context
	 * @return
	 */
	public static PMS getInstance (Context context) {
		CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
		if (instancePms == null) {
			instancePms = new PMS(context);
		}
		instancePms.setmContext(context);
		return instancePms;
	}

	public static PMSPopup getPopUpInstance () {
		return instancePmsPopup;
	}

	public void setPopupSetting (Boolean state, String title) {
		instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
	}

	/**
	 * clear
	 * 
	 * @return
	 */
	public static boolean clear () {
		try {
			PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
			instancePms.unregisterReceiver();
			instancePms = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * cust id 세팅
	 * 
	 * @param custId
	 */
	public void setCustId (String custId) {
		CLog.i("setCustId");
		CLog.d("setCustId:custId=" + custId);
		PMSUtil.setCustId(mContext, custId);
	}

	/**
	 * get cust id
	 * 
	 * @return
	 */
	public String getCustId () {
		CLog.i("getCustId");
		return PMSUtil.getCustId(mContext);
	}

	/**
	 * set debugTag
	 * 
	 * @param tagName
	 */
	public void setDebugTAG (String tagName) {
		CLog.setTagName(tagName);
	}

	/**
	 * set debug mode
	 * 
	 * @param debugMode
	 */
	public void setDebugMode (boolean debugMode) {
		CLog.setDebugMode(debugMode);
	}

	/**
	 * set file debug mode
	 *
	 * @param enable
	 */
	public void setDebugFileMode(boolean enable) {
		CLog.setDebugFileMode(enable);
	}

	public OnReceivePushListener getOnReceivePushListener () {
		return onReceivePushListener;
	}

	public void setOnReceivePushListener (OnReceivePushListener onReceivePushListener) {
		this.onReceivePushListener = onReceivePushListener;
	}

	/**
	 * get msg flag
	 * 
	 * @return
	 */
	public String getMsgFlag () {
		return mPrefs.getString(PREF_MSG_FLAG);
	}

	/**
	 * get noti flag
	 * 
	 * @return
	 */
	public String getNotiFlag () {
		return mPrefs.getString(PREF_NOTI_FLAG);
	}

	/**
	 * set noti icon
	 * 
	 * @param resId
	 */
	public void setNotiIcon (int resId) {
		mPrefs.putInt(PREF_NOTI_ICON, resId);
	}

	/**
	 * set large noti icon
	 * 
	 * @param resId
	 */
	public void setLargeNotiIcon (int resId) {
		mPrefs.putInt(PREF_LARGE_NOTI_ICON, resId);
	}

	/**
	 * set noti cound
	 * 
	 * @param resId
	 */
	public void setNotiSound (int resId) {
		mPrefs.putInt(PREF_NOTI_SOUND, resId);
	}

	/**
	 * set noti receiver
	 * 
	 * @param intentAction
	 */
	public void setNotiReceiver (String intentAction) {
		mPrefs.putString(PREF_NOTI_RECEIVER, intentAction);
	}

	/**
	 * set ring mode
	 * 
	 * @param isRingMode
	 */
	public void setRingMode (boolean isRingMode) {
		mPrefs.putString(PREF_RING_FLAG, isRingMode ? "Y" : "N");
	}

	/**
	 * get ring mode
	 *
	 */
	public Boolean getRingMode () {
		return mPrefs.getString(PREF_RING_FLAG).equals(FLAG_Y) ? true : false;
	}

	/**
	 * set vibe mode
	 * 
	 * @param isVibeMode
	 */
	public void setVibeMode (boolean isVibeMode) {
		mPrefs.putString(PREF_VIBE_FLAG, isVibeMode ? "Y" : "N");
	}

	/**
	 * get vibe mode
	 *
	 */
	public Boolean getVibeMode () {
		return mPrefs.getString(PREF_VIBE_FLAG).equals(FLAG_Y) ? true : false;
	}

	/**
	 * set popup noti
	 * 
	 * @param isShowPopup
	 */
	public void setPopupNoti (boolean isShowPopup) {
		mPrefs.putString(PREF_ALERT_FLAG, isShowPopup ? "Y" : "N");
	}

	/**
	 * set screen wakeup
	 * 
	 * @param isScreenWakeup
	 */
	public void setScreenWakeup (boolean isScreenWakeup) {
		mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, isScreenWakeup ? "Y" : "N");
	}

	/**
	 * set push popup activity
	 * 
	 * @param classPath
	 */
	public void setPushPopupActivity (String classPath) {
		mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, classPath);
	}

	/**
	 * set pushPopup showing time
	 * 
	 * @param pushPopupShowingTime
	 */
	public void setPushPopupShowingTime (int pushPopupShowingTime) {
		mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, pushPopupShowingTime);
	}

	public void bindBadge (Context c, int id) {
		Badge.getInstance(c).addBadge(c, id);
	}

	public void bindBadge (TextView badge) {
		Badge.getInstance(badge.getContext()).addBadge(badge);
	}

	/**
	 * show Message Box
	 * 
	 * @param c
	 */
	public void showMsgBox (Context c) {
		showMsgBox(c, null);
	}

	public void showMsgBox (Context c, Bundle extras) {
		CLog.i("showMsgBox");
		if (PhoneState.isAvailablePush()) {
			try {
				Class<?> inboxActivity = Class.forName(mPrefs.getString(PREF_INBOX_ACTIVITY));

				Intent i = new Intent(mContext, inboxActivity);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				if (extras != null) {
					i.putExtras(extras);
				}
				c.startActivity(i);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			// Intent i = new Intent(INBOX_ACTIVITY);
			// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// if (extras != null) {
			// i.putExtras(extras);
			// }
			// c.startActivity(i);
		}
	}

	public void closeMsgBox (Context c) {
		Intent i = new Intent(RECEIVER_CLOSE_INBOX);
		c.sendBroadcast(i);
	}

	public void badgeToNotReceiveCnt(boolean isUse)
	{
		String value = IPMSConsts.FLAG_N;
		if (isUse)
		{
			value = IPMSConsts.FLAG_Y;
		}

		mPrefs.putString(IPMSConsts.PREF_BADGE_NOT_RECEIVE_MSG_CNT, value);
	}

//	/**
//	 * start mqtt service
//	 */
//	public void startMQTTService (Context context) {
//		if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context))) {
//			context.sendBroadcast(new Intent(context, RestartReceiver.class).setAction(ACTION_START));
//		} else {
//			context.stopService(new Intent(context, MQTTService.class));
//		}
//	}
//
//	public void stopMQTTService (Context context) {
//		context.stopService(new Intent(context, MQTTService.class));
//	}

	/**
	 * set ServerUrl
	 * 
	 * @param serverUrl
	 */
	public void setServerUrl (String serverUrl) {
		CLog.i("setServerUrl");
		CLog.d("setServerUrl:serverUrl=" + serverUrl);
		PMSUtil.setServerUrl(mContext, serverUrl);
	}

	/**
	 * set inbox activity
	 * 
	 * @param inboxActivity
	 */
	public void setInboxActivity (String inboxActivity) {
		mPrefs.putString(PREF_INBOX_ACTIVITY, inboxActivity);
	}

	/*
	 * ===================================================== [start] database =====================================================
	 */
	/**
	 * select MsgGrp list
	 * 
	 * @return
	 */
	public Cursor selectMsgGrpList () {
		return mDB.selectMsgGrpList();
	}

	/**
	 * select MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public MsgGrp selectMsGrp (String msgCode) {
		return mDB.selectMsgGrp(msgCode);
	}

	/**
	 * select new msg Cnt
	 * 
	 * @return
	 */
	public int selectNewMsgCnt () {
		return mDB.selectNewMsgCnt();
	}

	/**
	 * select Msg List
	 * 
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row) {
		return mDB.selectMsgList(page, row);
	}

	/**
	 * select Msg List
	 * 
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgList (String msgCode) {
		return mDB.selectMsgList(msgCode);
	}

	/**
	 * select Msg
	 *
	 * @return
	 */
	public Msg selectMsgWhereMsgId (String msgId) {
		return mDB.selectMsgWhereMsgId(msgId);
	}

	/**
	 * select Msg
	 * 
	 * @param userMsgID
	 * @return
	 */
	public Msg selectMsgWhereUserMsgId (String userMsgID) {
		return mDB.selectMsgWhereUserMsgId(userMsgID);
	}

	/**
	 * select query
	 * 
	 * @param sql
	 * @return
	 */
	public Cursor selectQuery (String sql) {
		return mDB.selectQuery(sql);
	}

	public long insertDelMsg (String userMsgId) {
		DelMsg delMsg = new DelMsg();
		delMsg.userMsgId = userMsgId;
		return mDB.insertDelMsg(delMsg);
	}

	/**
	 * update MsgGrp
	 * 
	 * @param msgCode
	 * @param values
	 * @return
	 */
	public long updateMsgGrp (String msgCode, ContentValues values) {
		return mDB.updateMsgGrp(msgCode, values);
	}

	/**
	 * update read msg
	 * 
	 * @param msgGrpCd
	 * @param firstUserMsgId
	 * @param lastUserMsgId
	 * @return
	 */
	public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
	}

	/**
	 * update read msg where userMsgId
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId (String userMsgId) {
		return mDB.updateReadMsgWhereUserMsgId(userMsgId);
	}

	/**
	 * update read msg where msgId
	 * 
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId (String msgId) {
		return mDB.updateReadMsgWhereMsgId(msgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long deleteUserMsgId (String userMsgId) {
		return mDB.deleteUserMsgId(userMsgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param MsgId
	 * @return
	 */
	public long deleteMsgId (String MsgId) {
		return mDB.deleteMsgId(MsgId);
	}

	/**
	 * delete MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp (String msgCode) {
		return mDB.deleteMsgGrp(msgCode);
	}

	/**
	 * delete expire Msg
	 * 
	 * @return
	 */
	public long deleteExpireMsg () {
		return mDB.deleteExpireMsg();
	}

	/**
	 * delete empty MsgGrp
	 * 
	 * @return
	 */
	public void deleteEmptyMsgGrp () {
		mDB.deleteEmptyMsgGrp();
	}

	/**
	 * delete all
	 */
	public void deleteAll () {
		mDB.deleteAll();
	}

	/*
	 * ===================================================== [end] database =====================================================
	 */

	public void setNotiReceiverClass(String className) {
		mPrefs.putString(PREF_NOTI_RECEIVER_CLASS, className);
	}

	public void setPushReceiverClass(String className) {
		mPrefs.putString(PREF_PUSH_RECEIVER_CLASS, className);
	}

	public void setPushClickActivity(String action, String className){
		PMSUtil.setNotificationClickActivityAction(mContext, action);
		PMSUtil.setNotificationClickActivityClass(mContext, className);
	}

	public void setPushToken(String token){
		PMSUtil.setGCMToken(mContext, token);
	}

	public void createNotificationChannel(){
		PMSUtil.createNotificationChannel(mContext);
	}
}
