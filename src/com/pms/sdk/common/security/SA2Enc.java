package com.pms.sdk.common.security;

import com.pms.sdk.common.compress.CZip;

public class SA2Enc {

	private static String _USER_KEY = "amailRND2009";

	public static String generateKey (String key) {

		String freeKey = String.valueOf(20091022 * (Integer.parseInt(("" + System.currentTimeMillis()).substring(8)))) + "59483286493";
		String genKey = null;

		if (key == null) {
			genKey = freeKey.substring(0, 16);
		} else {

			if (key.length() > 16) {
				genKey = key.substring(0, 16);
			} else {
				genKey = key + (freeKey.substring(0, 16 - key.length()));
			}
		}
		return genKey;
	}

	public static String encode (String str, String userKey) {
		return encode(str, userKey, "UTF-8", true);
	}

	public static String encode (String str, String userKey, boolean useZip) {
		return encode(str, userKey, "UTF-8", useZip);
	}

	public static String encode (String str, String userKey, String charSet) {
		return encode(str, userKey, charSet, true);
	}

	public static String encode (String str, String userKey, String charSet, boolean useZip) {

		if (str == null || userKey == null || charSet == null)
			return null;

		String key = userKey.getBytes().length < 16 ? userKey + _USER_KEY.substring(0, 16 - userKey.getBytes().length) : userKey;

		try {
			byte[] enc2 = null;

			if (useZip) {
				byte[] enc1 = CZip.zipStringToBytes(str, charSet);

				enc2 = key == null ? Seed.seedEncrypt(enc1) : Seed.seedEncrypt(enc1, key.getBytes());
			} else {
				enc2 = key == null ? Seed.seedEncrypt(str.getBytes()) : Seed.seedEncrypt(str.getBytes(), key.getBytes());
			}

			return Base64.encodeBytes(enc2);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
