package com.pms.sdk.bean;

import org.json.JSONObject;

import android.database.Cursor;

public class DelMsg {

	public static final String TABLE_NAME = "TBL_DELMSG";
	public static final String _ID = "_id";
	public static final String USER_MSG_ID = "USER_MSG_ID";

	public static final String CREATE_MSG = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "( " + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + USER_MSG_ID
			+ " INTEGER " + ");";

	public String id = "-1";
	public String userMsgId = "";

	public DelMsg() {
	}

	public DelMsg(Cursor c) {
		id = c.getString(c.getColumnIndexOrThrow(_ID));
		userMsgId = c.getString(c.getColumnIndexOrThrow(USER_MSG_ID));
	}

	public DelMsg(JSONObject jo) {
		try {
			userMsgId = jo.getString("userMsgId");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
