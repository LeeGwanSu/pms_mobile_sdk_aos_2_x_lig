package com.pms.sdk.bean;

import android.database.Cursor;

public class Data {

	public static final String TABLE_NAME = "TBL_DATA";
	public static final String KEY = "KEY";
	public static final String VALUE = "VALUE";

	public static final String CREATE_DATA = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "( " + KEY + " TEXT, " + VALUE + " TEXT);";

	public String key = "";
	public String value = "";

	public Data() {

	}

	public Data(Cursor c) {
		key = c.getString(c.getColumnIndexOrThrow(KEY));
		value = c.getString(c.getColumnIndexOrThrow(VALUE));
	}
}
