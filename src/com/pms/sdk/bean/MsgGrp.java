package com.pms.sdk.bean;

import android.database.Cursor;

/**
 * @since 2012.12.26
 * @author erzisk
 * @description message group bean
 */
public class MsgGrp {

	public static final String TABLE_NAME = "TBL_MSG_GRP";
	public static final String _ID = "_id";
	public static final String USER_MSG_ID = "USER_MSG_ID";
	public static final String MSG_GRP_NM = "MSG_GRP_NM";
	public static final String MSG_TEXT = "MSG_TEXT";
	public static final String MSG_GRP_CD = "MSG_GRP_CD";
	public static final String MSG_ID = "MSG_ID";
	public static final String MSG_TYPE = "MSG_TYPE";
	public static final String NEW_MSG_CNT = "NEW_MSG_CNT";
	public static final String REG_DATE = "REG_DATE";

	public static final int ROW_COUNT = 30;

	public static final String CREATE_MSG_GRP = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "( " + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + USER_MSG_ID
			+ " INTEGER, " + MSG_GRP_NM + " TEXT, " + MSG_TEXT + " TEXT, " + MSG_GRP_CD + " TEXT, " + MSG_ID + " INTEGER, " + MSG_TYPE + " TEXT, "
			+ NEW_MSG_CNT + " INTEGER DEFAULT 0, " + REG_DATE + " TEXT " + ");";

	public String id = "-1";
	public String userMsgId = "";
	public String msgGrpNm = "";
	public String msgText = "";
	public String msgGrpCd = "";
	public String msgId = "";
	public String msgType = "";
	public String newMsgCnt = "0";
	public String regDate = "";

	public MsgGrp() {
	}

	public MsgGrp(Cursor c) {
		// Convert.cursorToBean(c, this);

		id = c.getString(c.getColumnIndexOrThrow(_ID));
		userMsgId = c.getString(c.getColumnIndexOrThrow(USER_MSG_ID));
		msgGrpNm = c.getString(c.getColumnIndexOrThrow(MSG_GRP_NM));
		msgText = c.getString(c.getColumnIndexOrThrow(MSG_TEXT));
		msgGrpCd = c.getString(c.getColumnIndexOrThrow(MSG_GRP_CD));
		msgId = c.getString(c.getColumnIndexOrThrow(MSG_ID));
		msgType = c.getString(c.getColumnIndexOrThrow(MSG_TYPE));
		newMsgCnt = c.getString(c.getColumnIndexOrThrow(NEW_MSG_CNT));
		regDate = c.getString(c.getColumnIndexOrThrow(REG_DATE));
	}

	// public MsgGrp(JSONObject jo) {
	// // Convert.jsonToBeanCamel(jo, this);
	//
	// try {id = jo.getString("id");} catch (Exception e) {e.printStackTrace();}
	// try {title = jo.getString("title");} catch (Exception e) {e.printStackTrace();}
	// try {msg = jo.getString("msg");} catch (Exception e) {e.printStackTrace();}
	// try {msgCode = jo.getString("msgCode");} catch (Exception e) {e.printStackTrace();}
	// try {msgId = jo.getString("msgId");} catch (Exception e) {e.printStackTrace();}
	// try {msgType = jo.getString("msgType");} catch (Exception e) {e.printStackTrace();}
	// try {newMsgCnt = jo.getString("newMsgCnt");} catch (Exception e) {e.printStackTrace();}
	// try {regDate = jo.getString("regDate");} catch (Exception e) {e.printStackTrace();}
	// }
}
